## Introduction

This directory contains the conda recipies for building [VTK 7.0](http://www.vtk.org) against Python 3.5 and Qt4 for Linux.  This recipes assume you have the following packages installed at the system level as there are not conda equivalents for them:

    fedora|redhat|centos:  gcc-c++ mesa-libGLU-devel libXmu-devel tbb-devel
    ubuntu|debian: g++ libglu1-mesa-dev libxmu-dev libtbb-dev


## Building Conda Recipes

To build the recipe, simply type:

    conda build recipe

If you then want to install from the local build:

    conda install --use-local <recipe package>

See [Conda Docs >> Building packages](http://conda.pydata.org/docs/building/build.html) for information on how to make, build and install a recipe, or just look at the plethora of examples on the [conda-recipes](https://github.com/conda/conda-recipes) github page.